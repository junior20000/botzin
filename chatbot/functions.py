#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 27/10/2020 16:08.

from abc import ABCMeta
from collections import Iterable
from datetime import datetime
from typing import Any, Dict, Union


class IFunction(metaclass=ABCMeta):
    name: str
    desc: str
    feature: bool
    available: bool

    def do(self, *args, **kwargs) -> Any:
        raise NotImplementedError


class FeatureManager:
    _functions: Dict[str, IFunction]

    @property
    def functions(self):
        return self._functions

    @functions.setter
    def functions(self, function: Union[IFunction, Iterable[IFunction]]):
        if isinstance(function, IFunction):
            self._functions[function.name] = function
        # TODO: TOO DIFFICULT TO READ!
        elif isinstance(function, Iterable):
            for func in function:
                if isinstance(func, IFunction):
                    self._functions[func.name] = func
        else:
            raise TypeError(
                f"<{type(function).__name__}> is not or do not contains a valid interceptor. Var. type must be <IFunction>")

    def set(self, function: Union[IFunction, Iterable[IFunction]]):
        self.functions = function


class MachineFunction(IFunction):
    def __init__(self, name: str, desc: str, feature: bool, available: bool):
        self.name = name
        self.desc = desc
        self.feature = feature
        self.available = available

    def do(self) -> Any:
        pass


class GreetDaytimeFunction(IFunction):
    name: str = "Greet daytime"
    key: str = "greet_daytime"  # TODO: CHANGE TYPE?
    feature: bool = False
    available: bool = True

    def __format__(self, format_spec):
        if format_spec == self.key:
            return self.do()

        return self.key

    def do(self) -> str:
        hour: int = datetime.now().hour

        if hour > 5:
            return "Bom dia"
        elif hour > 11:
            return "Boa Tarde"
        elif (hour < 6) and (hour > 17):
            return "Boa noite"
