#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 11/11/2020 22:17.
import json
from abc import ABCMeta, abstractmethod
from collections import defaultdict
from os import PathLike
from typing import ClassVar, Dict, DefaultDict, List, Optional, Sequence, Set, Type, Union

import nltk
from keras.models import Model, Sequential, load_model
from nltk.stem import StemmerI
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem.rslp import RSLPStemmer


# TODO: Maybe add strategy?
class INlp(metaclass=ABCMeta):
    __slots__ = ()

    language: str
    words: List[str]
    labels: List[str]
    document: DefaultDict[str, List[str]]
    model: Model


class INlpComponent(INlp):

    # TODO: MOVE TO STRATEGY
    @abstractmethod
    def _vectorize_words(self, patterns: Union[Sequence[str], Sequence[Sequence[str]]]) -> List[int]:
        pass


class NlpDataPlate(INlp):
    # TODO: DOCUMENT

    BLACKLIST: ClassVar[Set[str]] = {"?", "{", "}", "!", ".", ","}
    # TODO: Check if this will stay in this class
    __STEMMERS: Dict[str, Type[StemmerI]] = {"english": LancasterStemmer,
                                             "portuguese": RSLPStemmer}

    __slots__ = ("language", "stemmer", "words", "labels", "document", "context_info", "model", "corpus")

    def __init__(self, data_file: PathLike = "src/functions.json", language: str = "portuguese"):
        self.language: str = language
        # TODO: Raise custom exception to faulty option
        self.stemmer: StemmerI = NlpDataPlate.__STEMMERS[self.language]()
        # NLP properties
        self.words: List[str] = []
        self.labels: List[str] = []
        self.document: DefaultDict[str, List[str]] = defaultdict(
            list)  # OPTIMIZE: Maybe use a list of NamedTuples? / Should it stay here or in Trainer?
        self.context_info: DefaultDict[str, Optional[str]] = defaultdict(None)
        # DNN properties
        self.model: Model = Sequential()

        with open(data_file, encoding="utf-8") as file:
            self.corpus: Dict[str, Sequence[Dict[str, str]]] = json.load(file)

    def __repr__(self):
        # TODO: Implement!
        pass

    def start(self) -> None:
        """A high level function to prepare the data plate.

        Returns
        -------
        None

        """
        self.prepare_data()

    # TODO: CHANGE FUNCTION NAME!
    def prepare_data(self) -> None:
        """Sets the data up to be used.

        Returns
        -------
        None

        """

        # loop through each context cell
        for context_cell in self.corpus["context_cells"]:
            tag = context_cell["tag"]

            self.generate_context_info(tag, context_cell)
            self.feed_words_and_doc(tag, context_cell)
            self.labels.append(tag)
        # Post-feeding treatment
        self.word_cleaning()
        self.labels = sorted(self.labels)

    # TODO: CHANGE THE ARG NAME
    # TODO: CHANGE FUNCTION NAME
    # TODO: SPLIT FUNCTION IN TWO? (TOKENIZE, FEED)
    def feed_words_and_doc(self, tag: str, context_cell: Dict[str, str]) -> None:
        """Feeds words and document with the tokenized words.

        Parameters
        ----------
        tag : str
            The context to be populated in the document.
        context_cell : Dict[str, str]
            The context cell to be worked with.

        Returns
        -------
        None

        """

        for pattern in context_cell["patterns"]:
            wrd = nltk.word_tokenize(pattern, language=self.language)
            # store results
            self.words.extend(wrd)
            self.document[tag].append(wrd)

    def generate_context_info(self, tag: str, context_cell: Dict[str, str]):
        self.context_info['context'] = tag
        self.context_info['set'] = context_cell.get('context_set', None)
        self.context_info['filter'] = context_cell.get('context_filter', None)

    def word_cleaning(self) -> None:
        """Cleans the tokenized words with the chosen strategy.

        Returns
        -------
        None

        """
        self.words = [self.stemmer.stem(word.lower()) for word in self.words if word not in NlpDataPlate.BLACKLIST]
        self.words = sorted(list(set(self.words)))

    def load_model(self, model: PathLike) -> None:
        """Loads an already trained model.

        Parameters
        ----------
        model : PathLike
            Path to the trained model.

        Returns
        -------
        None

        """

        self.model = load_model(model)
