#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 11/11/2020 11:01.

from abc import ABCMeta, abstractmethod
from collections import Iterable
from typing import Any, Collection, List, Pattern, Union


class IInterceptor(metaclass=ABCMeta):
    __slots__ = ()

    context: Union[Collection[str], None]

    @abstractmethod
    def intercept(self, *args, **kwargs) -> Any:
        raise NotImplementedError


class IInterceptorRE(IInterceptor):
    target: Pattern

    @abstractmethod
    def intercept(self, string: str, *args, **kwargs) -> Any:
        raise NotImplementedError


class IInterceptorManager(metaclass=ABCMeta):
    def __init__(self, *interceptors: IInterceptor):
        self._interceptors: List[IInterceptor] = []
        self.interceptors = interceptors or InterceptorEmpty()

    @property
    def interceptors(self):
        return self._interceptors

    @interceptors.setter
    def interceptors(self, interceptor: Union[IInterceptor, List[IInterceptor]]):
        if isinstance(interceptor, IInterceptor):
            self._interceptors.append(interceptor)

        elif IInterceptorManager.is_iterable_and_only_contains_interceptors(interceptor):
            self._interceptors.extend(interceptor)

        else:
            raise TypeError(
                f"<{type(interceptor).__name__}> is not or do not contains a valid interceptor. Var. type must be <IInterceptor>")

    @classmethod
    def is_iterable_and_only_contains_interceptors(cls, interceptors: Collection[IInterceptor]):
        return isinstance(interceptors, Iterable) and all(
            isinstance(interceptor, IInterceptor) for interceptor in interceptors)

    @abstractmethod
    def intercept(self, msg: str, *args, **kwargs):
        raise NotImplementedError


class InterceptorEmpty(IInterceptor):

    def intercept(self, *args, **kwargs) -> Any:
        pass

    def process(self, *args, **kwargs) -> Any:
        pass
