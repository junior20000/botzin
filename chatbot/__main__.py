#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 24/11/2020 09:29.

from .nlp import NlpDataPlate
from .chatbot import ChatBot
from .train import Trainer

nlp_data = NlpDataPlate(data_file="src/functions3.json")
nlp_data.prepare_data()

STRING = "gostoso"
session = {"context": None}

print(nlp_data.words)

trainer = Trainer(nlp_data.stemmer, nlp_data.words, nlp_data.labels, nlp_data.document)
trainer.start()
trainer.fit(times=2, verbose=1)
trainer.model.save("model9.h5")

nlp_data.load_model("model9.h5")

cb = ChatBot(nlp_data.language, nlp_data.stemmer, nlp_data.words, nlp_data.labels, nlp_data.corpus, nlp_data.model, confidence_level=0.1)

print(cb.classify(STRING))
print(cb.respond(STRING, get_context=True, session=session))
