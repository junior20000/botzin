#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 24/11/2020 09:29.

from os import PathLike
from typing import DefaultDict, List, Sequence, NoReturn

import numpy as np
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.layers import Dense, Dropout, InputLayer
from keras.models import Model, Sequential, load_model
from keras.optimizers import SGD
from nltk.stem import StemmerI

from .nlp import INlpComponent


class Trainer(INlpComponent):

    def __init__(self, stemmer: StemmerI, words: List[str], labels: List[str], document: DefaultDict[str, List[str]],
                 language: str = "portuguese"):
        self.language = language
        # private properties
        self.__stemmer: StemmerI = stemmer  # TODO: Raise custom exception to faulty option
        # pre-training properties
        self.words: List[str] = words
        self.labels: List[str] = labels
        self.document: DefaultDict[str, List[str]] = document
        # training properties
        self.x_train: List[List[int]] = []
        self.y_train: List[List[int]] = []
        # DNN properties
        self.model: Model = Sequential()

    def _vectorize_words(self, sentences: Sequence[str]) -> List[int]:
        # generate stemmed words from sentences
        p_words = [self.__stemmer.stem(word.lower()) for sentence in sentences for word in sentence]

        return [1 if word in p_words else 0 for word in self.words]

    def start(self) -> None:
        """A high level function to set the trainer up.

        Returns
        -------
        None

        """
        self.generate_training_data()
        self.model_dnn()
        self.train()

    def generate_training_data(self) -> None:
        """Generates training data.
        Generates training data vectoring the X and Y data.

        Returns
        -------
        None

        """

        empty_tag_vector = [0] * len(self.labels)

        for context, patterns in self.document.items():
            bow = self._vectorize_words(patterns)

            tag_vector = empty_tag_vector[:]
            tag_vector[self.labels.index(context)] = 1

            self.x_train.append(bow)
            self.y_train.append(tag_vector)

    def model_dnn(self) -> None:
        """Sets the dnn model up.

        Returns
        -------
        None

        """

        self.model.add(Dense(256, input_shape=(len(self.x_train[0]),), activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(64, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(len(self.y_train[0]), activation='softmax'))

    def load_model(self, model: PathLike, compile_model=False) -> NoReturn:
        """Loads an already trained model.

        Parameters
        ----------
        model : PathLike
            Path to the trained model.

        Returns
        -------
        None

        """

        self.model = load_model(model, compile=compile_model)

    def train(self, epochs: int = 30000, batch_size: int = 512, verbose: int = 0) -> None:
        """Trains the model.

        Parameters
        ----------
        epochs : int
            Number of epochs.
        batch_size : int
            Batch size.
        verbose : int
            Verbose level.

        Returns
        -------
        None

        """

        # TODO: OPTIMIZER STRATEGY
        sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        self.model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

        self.fit(epochs=epochs, batch_size=batch_size, verbose=verbose)

    def fit(self, epochs: int = 30000, batch_size: int = 512, verbose: int = 0, times=1) -> None:
        """Fits the model.

        Parameters
        ----------
        epochs : int
            Number of epochs.
        batch_size : int
            Batch size.
        verbose : int
            Verbose level.
        times : int
            Number of times to fit the model.

        Returns
        -------
        None

        """

        # TODO: RAISE EXCEPTION IF TIMES = 0
        # TODO: SPLIT FUNCTIONS TO MODEL CHECKPOINT AND EARLY STOPPING?
        mc = ModelCheckpoint(filepath="tmp/weights.{epoch:02d}-{val_loss:2f}.hdf5", monitor="val_loss", verbose=0,
                             save_best_only=True, save_weights_only=True, mode="auto", save_freq="epoch")

        es = EarlyStopping(
            monitor='val_loss',
            min_delta=0,
            patience=100,
            verbose=0,
            mode='auto',
            restore_best_weights=True
        )

        reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                      patience=100, verbose=1, mode='auto',
                                      min_delta=0.0001, cooldown=0, min_lr=1e-8)

        # stps_per_epoch = len(self.data_bow[0]) // batch_size

        for x in range(0, times):
            self.model.fit(np.array(self.x_train), np.array(self.y_train), epochs=epochs, batch_size=batch_size,

                           #validation_data=(np.array(self.x_train), np.array(self.y_train)), callbacks=[mc, es, reduce_lr],
                           verbose=verbose, shuffle=True)
            print(f"{x} / {times}", end="\r")
