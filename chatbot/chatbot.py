#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 11/11/2020 22:13.

import random
from typing import Sequence, List, Union, Tuple

import nltk
from keras.models import Model
from nltk.stem import StemmerI

from .nlp import INlpComponent


# TODO: BECOME SCALABLE!
class ChatBot(INlpComponent):

    def __init__(self,
                 language: str,
                 stemmer: StemmerI,
                 words: List[str],
                 labels: List[str],
                 corpus: dict,
                 model: Model,
                 confidence_level: float = 0.9):
        self.language: str = language
        self.stemmer: StemmerI = stemmer
        # Classification properties
        self.words: List[str] = words
        self.labels: List[str] = labels
        self.corpus: dict = corpus
        # DNN related properties
        self.model: Model = model
        self.confidence_level: float = confidence_level

    def _vectorize_words(self, words: Sequence[Sequence[str]]) -> List[int]:
        # Initialize bow
        bow = [0] * len(self.words)
        # Populate bow
        for word in words:
            for i, w in enumerate(self.words):
                if w == word:
                    bow[i] = 1

        return bow

    def classify(self, string: str):
        # generate probabilities from the model
        s_words = nltk.word_tokenize(string, language=self.language)
        s_words = [self.stemmer.stem(word.lower()) for word in s_words]

        bow = self._vectorize_words(s_words)

        results = self.model.predict([bow], workers=1, verbose=0)[0]
        # filter out predictions below a threshold, and provide intent index
        results = [[i, r] for i, r in enumerate(results) if r > self.confidence_level]
        # sort by strength of probability
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append((self.labels[r[0]], str(r[1])))
        # return tuple of intent and probability
        return return_list

    def respond(self, string: str, get_context = False, session: dict = None) -> Union[str, Tuple[str, Tuple[str, str]]]:
        prediction = self.classify(string)

        if not prediction:
            return "Perdão, não entendi." if not get_context else (session['context'], "Perdão, não entendi.")

        for context_cell in self.corpus['context_cells']:
            if prediction[0][0] != context_cell['tag']:
                continue

            if "context_set" in context_cell:
                session['context'] = None if (context_cell['context_set'] == "") else context_cell['context_set']

            if not "context_filter" in context_cell or (session['context'] == context_cell['context_filter']):
                return random.choice(context_cell['responses']) if not get_context else (context_cell['tag'], random.choice(context_cell['responses']))


        #responseList = list(filter(lambda x: x['tag'] == prediction[0][0], self.corpus['context_cells']))
        #return random.choice(responseList[0]['responses']) if not get_context else (responseList[0]['tag'], random.choice(responseList[0]['responses']))
