#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 19/11/2020 19:39.

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.sqlite import BOOLEAN

db: SQLAlchemy = SQLAlchemy(session_options={"autoflush": False})


class CarModel(db.Model):
    __tablename__ = "CarModel"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    name = db.Column('name', db.TEXT, nullable=False, unique=True)
    cost = db.Column('cost', db.REAL, nullable=False)

    cars = db.relationship("Car", backref='car_model', lazy='dynamic')


class Car(db.Model):
    __tablename__ = "Car"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    model_id = db.Column('model_id', db.Integer, db.ForeignKey('CarModel.id'), nullable=False)
    plate = db.Column('plate', db.TEXT, nullable=False, unique=True)

    rentals = db.relationship("Rental", backref='car', lazy='subquery')


class Rental(db.Model):
    __tablename__ = "Rental"

    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    car_id = db.Column('car_id', db.Integer, db.ForeignKey('Car.id'), nullable=False)
    name = db.Column('name', db.TEXT, nullable=False)
    insurance = db.Column('insurance', BOOLEAN, nullable=False, default=0)
    start_date = db.Column('startDate', db.DATETIME, nullable=False)
    end_date = db.Column('endDate', db.DATETIME, nullable=False)