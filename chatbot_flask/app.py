#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 24/11/2020 10:21.

import time
import datetime
from random import choice

from flask import Flask, request, render_template, session
from flask_session import Session

from chatbot.chatbot import ChatBot
from chatbot.nlp import NlpDataPlate
from chatbot_flask.formatter import ChatFormatter
from chatbot_flask.interceptor import InterceptorManager, InterceptorCEP, InterceptorCurrency, InterceptorCarRent
from chatbot_flask.models import db

t1 = time.time()
date = datetime.datetime.today().strftime('%d-%m-%Y %H:%M:%S')

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///D:\\Projetos\\chatbot_flask\\db\\db.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SESSION_PERMANENT'] = True
app.config['SESSION_TYPE'] = "filesystem"
app.config['SESSION_FILE_DIR'] = "sessions"
app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(hours=5)
app.config['SESSION_FILE_THRESHOLD'] = 150
app.config['SECRET_KEY'] = "pt30WMbpqLrZB61eGGK3kLXo_BxwKhJerdgXsm0ugcg"

sess = Session()
sess.init_app(app)
db.init_app(app)

nlp_data = NlpDataPlate(data_file="src/functions3.json")
nlp_data.prepare_data()
nlp_data.load_model("model9.h5")

chat_f = ChatFormatter()
im = InterceptorManager(InterceptorCEP(), InterceptorCurrency(), InterceptorCarRent())

cb = ChatBot(nlp_data.language, nlp_data.stemmer, nlp_data.words, nlp_data.labels, nlp_data.corpus, nlp_data.model,
             confidence_level=0.20)


@app.route('/')
def hello_world():
    return "index.html"


@app.route("/chatbot", methods=["GET"])
def chatbot():
    #session['context'] = "CarRent_GetClientName"
    session['context'] = None
    return render_template("chat.html")


@app.route('/chat-input', methods=["GET"])
def bot_response():
    text: str = request.args.get("msg")

    if not "context" in session:
        session['context'] = None

    if response := im.intercept(text, session=session):
        return chat_f.convert_eol(response[1])

    response = cb.respond(text, get_context=True, session=session)

    response = f"{response[1]}".format(name="Botzin",
                                       time=str(time.time() - t1) + " segundos",
                                       creator_name="Roberto Schiavelli Júnior",
                                       day_time=date,
                                       version_1_date_release="ainda não saiu kkkkk",
                                       actions_list="A minha lista de ações vem aqui!",
                                       exclamation="OK",
                                       _func_random_joy_emoji=random_joy_emoji())

    return response


def random_joy_emoji():
    emojis = ["(* ^ ω ^)",
              "(o^▽^o)",
              "ヽ(・∀・)ﾉ",
              "(o･ω･o)",
              "(^人^)",
              "╰(▔∀▔)╯",
              "＼(＾▽＾)／",
              "☆*:.｡.o(≧▽≦)o.｡.:*☆",
              "٩(◕‿◕)۶",
              "(๑˃ᴗ˂),ﻭ"
              "｡ﾟ(TヮT)ﾟ｡",
              "	(*￣▽￣)b"]

    return choice(emojis)

if __name__ == '__main__':
    app.run(debug=True)
