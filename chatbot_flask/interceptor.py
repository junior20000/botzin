#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 24/11/2020 10:09.

import re
from datetime import datetime
from typing import Any, Match, Pattern, Optional, Tuple

from sqlalchemy.sql import func
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import load_only

from api.awesome_api import CEPAPI, CEP, CurrencyAPI
from chatbot.interceptor import IInterceptor, IInterceptorManager, IInterceptorRE
from chatbot_flask.context import ContextCarRent
from chatbot_flask.misc.distance import DistanceCalculator, HaversineDistance
from chatbot_flask.models import Car, CarModel, Rental, db


class InterceptorManager(IInterceptorManager):

    def __init__(self, *interceptors: IInterceptor):
        super().__init__(*interceptors)

    def intercept(self, msg: str, /, *args, session: dict = None):
        for interceptor in self._interceptors:
            if response := interceptor.intercept(msg, session=session):
                return response

        return None


class InterceptorCEP(IInterceptorRE):

    def intercept(self, msg: str, /, *args, **kwargs) -> Optional[Tuple[None, str]]:

        if match := self._case_delivery(msg):
            cep_obj = self._get_cep_from_match(match)

            dist_calc = DistanceCalculator(-23.6103482, -46.6107422).set_distance_type(HaversineDistance())
            distance = dist_calc.get_distance((cep_obj.lat, cep_obj.lng))

            return (None,
                    f"O seu endereço está dentro da zona de entrega!\n - Distância: {distance:.2f}km\n - Método: {dist_calc.distance_type.name}") if distance <= 30 else (
                None, f"Sinto muito, mas o seu endereço está fora da zona de entrega por {(distance - 30):.2f}km")

        elif match := self._case_about_cep(msg):
            cep_obj = self._get_cep_from_match(match)

            return (None, repr(cep_obj))

        return None

    def _case_delivery(self, text: str) -> Optional[Match]:
        pattern = re.compile(
            r"(?:delivery|entrega(?:riam|ram|rão|r|s|m)).{0,10}cep(?::)? *(?P<CEP>(?:\d{8}|\d{5}(?:-|.)\d{3}))")

        return re.search(pattern, text)

    def _case_about_cep(self, text: str) -> Optional[Match]:
        pattern = re.compile(
            r"(?i)(?:sobre|infor(?:mar|mações)) .{0,40}cep(?::)? *(?P<CEP>(?:\d{8}|\d{5}(?:-|.)\d{3}))")

        return re.search(pattern, text)

    def _get_cep_from_match(self, cep_match: Match) -> CEP:
        cep = cep_match.group("CEP").replace("-", "")
        cep_api = CEPAPI(cep)

        return cep_api.get()


class InterceptorCurrency(IInterceptorRE):
    pattern: Pattern = re.compile(r"^(?i)cotação ?(?P<currency>[^\d]*)$")

    def intercept(self, msg: str, *args, **kwargs) -> Any:
        match = re.search(self.pattern, msg)

        if match:
            return self.process(match.group("currency"))

        return None

    def process(self, currency: str) -> Any:  # TODO: DEPRECIATED
        c_api = CurrencyAPI()

        currencies: dict = {"dolar": c_api.get_usd,
                            "dólar": c_api.get_usd,
                            "dolar turismo": c_api.get_usdt,
                            "dólar turismo": c_api.get_usdt,
                            "dolar canadense": c_api.get_cad,
                            "dólar canadense": c_api.get_cad,
                            "euro": c_api.get_euro,
                            "libra": c_api.get_gbp,
                            "libra esterlina": c_api.get_gbp,
                            "peso": c_api.get_ars,
                            "peso argentino": c_api.get_ars,
                            "iene": c_api.get_jpy,
                            "iene japonês": c_api.get_jpy,
                            "franco": c_api.get_chf,
                            "franco suíço": c_api.get_chf,
                            "yuan": c_api.get_cny,
                            "yuan chinês": c_api.get_cny,
                            "shekel": c_api.get_yls,
                            "novo shekel israelense": c_api.get_yls,
                            "bitcoin": c_api.get_bitcoin,
                            "btc": c_api.get_bitcoin,
                            "litecoin": c_api.get_litecoin,
                            "ltc": c_api.get_litecoin,
                            "ethereum": c_api.get_ethereum,
                            "eth": c_api.get_ethereum,
                            "ripple": c_api.get_ripple,
                            "xrp": c_api.get_ripple
                            }

        currency = currency.lower().strip()
        msg = "{currency}: R${value:.2f}"

        response = currencies.get(currency, None)

        if response:
            response = response()

            if "status" in response:
                msg = "Moeda não disponível, perdão (T_T)"
            else:
                msg = msg.format(currency=currency.upper(), value=float(response[0]["ask"]))

        else:
            msg = "Moeda não encontrada, desculpa :p"

        return (None, msg)


class InterceptorCarRent(IInterceptorRE):

    def intercept(self, string: str, *args, session: dict = None) -> Any:
        string = string.strip()
        context = session['context']

        db.session.flush()

        if re.search("(?i)listar (?:carros|ve[íi]culos)", string):
            cars = db.session.query(Car).join(CarModel).order_by(CarModel.name).all()

            return (context, '<hr style="margin-top:0.5em;margin-bottom:0.5em">'.join([f"Chapa: {car.plate}\nModel: {car.car_model.name}\nCusto: R${car.car_model.cost} / dia\n" for car in cars]))

        elif re.search("(?i)listar modelos", string):
            model_names = db.session.query(CarModel.name).order_by(CarModel.name).all()

            return (context, '<hr style="margin-top:0.5em;margin-bottom:0.5em">'.join(name for model in model_names for name in model))

        if context == "CarRent_GetClientName":
            if not string.replace(" ", '').isalpha():
                return (context, "Sinto muito, mas, o seu nome só pode conter letras")

            string = string.title()

            session['client_name'] = string
            session['context'] = "CarRent_GetCarModel"

            return ("CarRent_GetCarModel",
                    f"Perfeito! {string}, por favor, insira o modelo do veículo que deseja alugar.\n<b>Dica</b>: Para obter a lista de carros disponíveis, digite <i><b>\"listar carros\"</b></i>\nPara modelos, digite <i><b>\"listar modelos\"</b></i>")

        elif context == "CarRent_GetCarModel":
            if cars := db.session.query(Car).join(CarModel).filter(func.lower(CarModel.name) == string.lower()).all():
                session['cars'] = cars
                session['context'] = "CarRent_GetStartDate"
                return ("CarRent_GetStartDate",
                        f"Excelente! Temos <b>{cars[0].car_model.name}s</b> disponíveis.<br>Por favor, digite a data (dd/mm/aaaa) de quando desejas retirar o veículo")

            return (context, f"Perdão, mas temo não ter conseguido localizar nenhum veículo de modelo {string}")

        elif context == "CarRent_GetStartDate":
            p_date = re.compile(r"(?P<day>[0-9]{2})/(?P<month>[0-9]{2})/(?P<year>[0-9]{4}$)")
            match = re.search(p_date, string)

            if re.search("(?i)hoje|agora|nesse instante", string):
                return (
                context, "Para realizar o aluguel hoje, entre em contato pelo número (11) 7070-7070 ou presencial")

            if not match or not self._validate_date(match.group()):
                return (context, "Perdão, mas a data digitada não é válida")

            start_date = datetime.strptime(match.group(), "%d/%m/%Y")

            if start_date.date() < datetime.now().date():
                return (context, "Pretendes alugar o veículo no passado, viajante do tempo?")
            elif datetime.now().date() == start_date.date():
                return (
                context, "Para realizar o aluguel hoje, entre em contato pelo número (11) 7070-7070 ou presencial")

            db.session.add_all(session['cars'])
            map(db.session.refresh, session['cars'])

            for car in session['cars']:
                if all(not self._check_date_overlap(start_date.strftime("%d/%m/%Y"),
                                                    start_date.strftime("%d/%m/%Y"),
                                                    rental.start_date.strftime("%d/%m/%Y"),
                                                    rental.end_date.strftime("%d/%m/%Y")) for rental in car.rentals):
                    session['car'] = car
                    break
            else:
                return (context,
                        "Sinto muito, mas o período escolhido colide com o de algum outro aluguel.\nPor favor, digite uma nova data de retirada")

            session['start_date'] = start_date
            session['context'] = "CarRent_GetEndDate"
            return ("CarRent_GetEndtDate", "Ótimo! Digite a data de devolução do veículo")

        elif context == "CarRent_GetEndDate":
            p_date = re.compile(r"(?P<day>[0-9]{2})/(?P<month>[0-9]{2})/(?P<year>[0-9]{4}$)")
            match = re.search(p_date, string)

            if match and self._validate_date(match.group()):
                start_date = session['start_date']
                end_date = datetime.strptime(match.group(), "%d/%m/%Y")

                if end_date < start_date:
                    return (context,
                            f"Perdão, mas a data de entrega ({end_date.date()}) não pode ser <b>antes</b> da retirada ({start_date.date()})")
                elif end_date == start_date:
                    return (context,
                            f"Perdão, mas a data de entrega ({end_date.date()}) não pode ser a <b>mesma</b> da retirada ({start_date.date()})")

                db.session.add_all(session['cars'])
                map(db.session.refresh, session['cars'])

                for car in session['cars']:
                    if all(not self._check_date_overlap(session['start_date'].strftime("%d/%m/%Y"), end_date.strftime("%d/%m/%Y"), rental.start_date.strftime("%d/%m/%Y"),
                                                        rental.end_date.strftime("%d/%m/%Y")) for rental in car.rentals):
                        session['car'] = car
                        break
                else:
                    return (context,
                            "Sinto muito, mas o período escolhido colide com o de algum outro aluguel.\nPor favor, digite uma nova data de entrega")

                session['end_date'] = end_date
                session['context'] = "CarRent_Insurance"

                return ("CarRent_Insurance",
                        "Ótimo! Só mais uma pergunta antes de finalizarmos, você gostaria de incluir um seguro?")

            return (context, "Perdão, mas a data digitada não é válida")

        elif context == "CarRent_Insurance":
            if re.search(
                    "(?i)^(?:eu )?(?:com certeza )?(?:quero|gostaria|aceit(?:o|aria))(?: muito)?(?: sim)?(?:,? por favor)?$|^(?:sim|claro(?: que sim)?|com certeza(?: sim| aceito)?)$",
                    string):

                db.session.add(session['car'])
                db.session.refresh(session['car'])

                rental = Rental(car_id=session['car'].id,
                            name=session['client_name'],
                            insurance=True,
                            start_date=datetime.strptime(datetime.strftime(session['start_date'], '%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S'),
                            end_date=datetime.strptime(datetime.strftime(session['end_date'], '%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S'))
                db.session.add(rental)
                message = f"Perfeito! Aqui está o resumo da sua compra:\n * Nome: {session['client_name']}\n * Chapa: {session['car'].plate}\n * Modelo: {session['car'].car_model.name} (R${session['car'].car_model.cost} / dia)\n * Seguro: Sim (0% de desconto)\n * Valor total: R${session['car'].car_model.cost * (session['end_date'] - session['start_date']).days:,.2f}"

                try:
                    db.session.commit()
                except IntegrityError as ie:
                    session['context'] = "CarRent_GetEndDate"
                    return ("CarRent_GetStartDate",
                     "Sinto muito, mas o período escolhido colide com o de algum outro aluguel.\nPor favor, digite uma nova data de retirada")

                db.session.expire_all()

                session['context'] = None
                session['client_name'] = None
                session['car'] = None
                session['start_date'] = None
                session['end_date'] = None

                return (None,
                        message)

    def _validate_date(self, date_str: str):
        try:
            datetime.strptime(date_str, '%d/%m/%Y')
        except:
            return False

        return True

    def _check_date_overlap(self, start1: str, end1: str, start2: str, end2: str):
        start1, end1, start2, end2 = (datetime.strptime(x, "%d/%m/%Y") for x in (start1, end1, start2, end2))

        if (start1 <= end2) and (end1 >= start2):
            return True

        return False

    def _date_to_iso_8601(self,
                          day: str,
                          month: str,
                          year: str,
                          hour: str = "00",
                          min: str = "00",
                          seg: str = "00"):

        return f"{year}-{month}-{day} {hour}:{min}:{seg}"
