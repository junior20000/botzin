#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 03/11/2020 20:31.

from string import Formatter

class ChatFormatter(Formatter):
    def format_field(self, value, format_spec):
        if format_spec.endswith("bold"):
            value = f"<b>{value}</b>"
            format_spec = format_spec[:-4]
        elif format_spec.endswith("strong"):
            value = f"<strong>{value}</strong>"
            format_spec = format_spec[:-6]
        elif format_spec.endswith("italic"):
            value = f"<i>{value}</i>"
            format_spec = format_spec[:-6]

        return super(ChatFormatter, self).format(value, format_spec)

    def convert_eol(self, string: str, /) -> str:
        return string.replace("\n", "<br>")