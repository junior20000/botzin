#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 10/11/2020 09:24.

from typing import ClassVar, Dict

class ChatSession:
    session_storage: ClassVar[Dict["str", "str"]]

    def new_session(self):
        pass