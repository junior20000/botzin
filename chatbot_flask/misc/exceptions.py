#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 18/10/2020 08:45.

class DistanceTypeNotSetException(Exception):
    def __init__(self, message="Distance type not set!"):
        self.message: str = message

    def __repr__(self):
        return self.message

class LatitudeOutOfBoundsException(Exception):
    def __init__(self, message="The latitude should be between -90° and 90°"):
        self.message: str = message

    def __repr__(self):
        return self.message

class LongitudeOutOfBoundsException(Exception):
    def __init__(self, message="The longitude should be between -180° and 180°"):
        self.message: str = message

    def __repr__(self):
        return self.message