#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 03/11/2020 22:38.

from abc import ABCMeta, abstractmethod
from math import acos, cos, radians, sin
from typing import ClassVar, Optional, Tuple

from .exceptions import *


class DistanceCalculator:

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y
        self._distance_type: "IDistanceType" = EmptyDistance()

        self.validate_coordinates(x, y)

    @property
    def distance_type(self) -> "IDistanceType":
        return self._distance_type

    @distance_type.setter
    def distance_type(self, distance_type: "IDistanceType") -> None:
        if not isinstance(distance_type, IDistanceType):
            raise TypeError(
                f"<{type(distance_type).__name__}> is not a valid strategy. Var. type must be <IDistanceType>")

        self._distance_type = distance_type

    def set_distance_type(self, distance_type: "IDistanceType") -> "DistanceCalculator":
        self.distance_type = distance_type

        return self

    def get_distance(self, destination: Tuple[float, float]):
        return self.distance_type.get_distance((self.x, self.y), destination)

    def get_new_distance(self, origin: Tuple[float, float], destination: Tuple[float, float]):
        return self.distance_type.get_distance(origin, destination)

    def validate_coordinates(self,
                             lat1: float,
                             lng1: float,
                             lat2: Optional[float] = None,
                             lng2: Optional[float] = None):
        self._distance_type.validate_coordinates(lat1, lng1, lat2, lng2)


class IDistanceType(metaclass=ABCMeta):
    name: ClassVar[str]

    @abstractmethod
    def get_distance(self, *args, **kwargs) -> float:
        raise NotImplementedError

    # TODO: CHANGE TO DECIMAL 6 PRECISION POINTS?
    # TODO: TOO DIFFICULT TO READ! BREAK IT!
    def validate_coordinates(self,
                             lat1: float,
                             lng1: float,
                             lat2: Optional[float] = None,
                             lng2: Optional[float] = None) -> None:
        if not (-90 <= (lat1 or lat2) <= 90) or not (-90 <= (lat2 or lat1) <= 90):
            raise LatitudeOutOfBoundsException
        elif not (-180 <= (lng1 or lng2) <= 180) or not (-180 <= (lng2 or lng1) <= 180):
            raise LongitudeOutOfBoundsException


class EmptyDistance(IDistanceType):

    def get_distance(self, *args, **kwargs) -> float:
        raise DistanceTypeNotSetException


class IGeodesy(metaclass=ABCMeta):
    R: ClassVar[int] = 6371  # Earth radius in Km
    RADIAN: ClassVar[float] = 57.2957795131  # 180 / π (pi)


class HaversineDistance(IDistanceType, IGeodesy):
    name = "Haversine"

    def central_angle(self, d_lat, d_lng, lat1, lat2) -> float:
        return self.hav(d_lat) + cos(lat1) * cos(lat2) * self.hav(d_lng)

    def hav(self, central_angle: float) -> float:
        return (sin(central_angle / 2)) ** 2

    def arcversin(self, y: float) -> float:
        return acos(1 - y)

    def archaversin(self, y: float) -> float:
        return acos(1 - 2 * y)

    # TODO: CREATE COORDINATES OBJECT / NamedTuple?
    def get_distance(self, origin: Tuple[float, float], destination: Tuple[float, float]) -> float:
        self.validate_coordinates(*origin + destination)
        # Convert to radians
        lat1, lng1, lat2, lng2 = map(radians, origin + destination)
        # Get delta
        d_lat: float = lat2 - lat1
        d_lng: float = lng2 - lng1
        # Get haversine of the central angle -> hav(Θ)
        h = self.central_angle(d_lat, d_lng, lat1, lat2)

        return self.archaversin(h) * self.R


class SphericalLawOfCosinesDistance(IDistanceType, IGeodesy):
    name = "Spherical Law Of Cosines"

    def get_distance(self, origin: Tuple[float, float], destination: Tuple[float, float]) -> float:
        self.validate_coordinates(*origin + destination)

        lat1, lng1 = origin
        lat2, lng2 = destination
        # Convert to radians
        lat1, lat2, lng1, lng2 = map(radians, (lat1, lat2, lng1, lng2))
        # Get delta
        d_lng: float = lng2 - lng1

        return acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(d_lng)) * self.R
