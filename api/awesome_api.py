#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 10/10/2020 17:00.

import re
import xml.etree.ElementTree as ET
from typing import Union

from requests import Response, request

_BASE_URL: str = r"https://{target}.awesomeapi.com.br"


class CEPAPI:
    __slots__ = ("cep", "format", "target")

    def __init__(self, cep: str, format: str = "json"):
        self.cep: str = cep
        self.format: str = format

        self.target: str = "cep"

    # TODO: MAYBE RETURN <CEPInfo> class?
    def get(self, return_format: str = "cep_obj") -> Union[dict, ET.Element, "CEP"]:
        if not re.match(r"\d{8}", str(self.cep)):
            # TODO: RAISE CUSTOM EXCEPTION
            raise Exception

        response: Response = request("get", f"{_BASE_URL.format(target=self.target)}/{self.format}/{self.cep}")

        # TODO: ENUM POSSIBLE FORMATS?
        # TODO: REMOVE IF-ELSE
        if return_format == "dict":
            data: dict = response.json()
        elif return_format == "xml":
            data: ET.Element = ET.fromstring(response.text)
        elif return_format == "cep_obj":
            data: CEP = CEP().populate_from_dict(response.json())
        else:
            # TODO: RAISE CUSTOM EXCEPTION
            raise Exception

        return data


# TODO: CHANGE NAME TO <ADDRESS>?
class CEP:
    __slots__ = ("cep",
                 "address_type",
                 "address_name",
                 "address",
                 "district",
                 "state",
                 "city",
                 "city_ibge",
                 "lat",
                 "lng",
                 "ddd")

    # TODO: REFACTOR WITH KWARGS
    def __init__(self,
                 cep: str = "",
                 address_type: str = "",
                 address_name: str = "",
                 address: str = "",
                 district: str = "",
                 state: str = "",
                 city: str = "",
                 city_ibge: int = 0,
                 lat: float = .0,
                 lng: float = .0,
                 ddd: int = 0):
        self.cep: str = cep
        self.address_type: str = address_type
        self.address_name: str = address_name
        self.address: str = address

        self.district: str = district
        self.state: str = state
        self.city: str = city
        self.city_ibge: int = city_ibge

        self.lat: float = lat
        self.lng: float = lng

        self.ddd: int = ddd

    def __repr__(self):
        return "\n".join((f"{slot.capitalize().replace('_', ' ')}: {getattr(self, slot)}") for slot in self.__slots__)

    def __format__(self, format_spec):
        # TODO: REMOVE HIGH COUPLING
        return getattr(self, format_spec, "N/A")

    def __iter__(self):
        for slot in self.__slots__:
            yield slot, getattr(self, slot, None)

    def __eq__(self, other: "CEP"):
        """Checks the equality of two CEP instances.

        Parameters
        ----------
        other: CEP
            A <CEP> instance object.

        Returns
        -------
        True if both objects has the same hash, False otherwise.

        """
        if isinstance(other, CEP):
            return hash(self) == hash(other)

        return False

    def __hash__(self):
        return hash(self.cep)

    def asdict(self) -> dict:
        return dict(self)

    def populate_from_dict(self, response: dict) -> "CEP":
        for k, v in response.items():
            type_attr = type(getattr(self, k))
            c_value = type_attr(v)
            setattr(self, k, c_value)

        return self


# TODO: ADD CURRENCY OBJECT
# TODO: ADD STRATEGY
class CurrencyAPI:

    @staticmethod
    def get_currencies():
        pass

    # TODO IMPLEMENT ASAP
    def get(self, format: str = "json", **kwargs):
        pass

    def get_all(self):
        pass

    # TODO: BETTER!

    def get_usd(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/USD-BRL/{quantity}")

        return response.json()

    def get_usdt(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/USDT-BRL/{quantity}")

        return response.json()

    def get_cad(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/CAD-BRL/{quantity}")

        return response.json()

    def get_aud(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/AUD-BRL/{quantity}")

        return response.json()

    def get_euro(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/EUR-BRL/{quantity}")

        return response.json()

    def get_gbp(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/GBP-BRL/{quantity}")

        return response.json()

    def get_ars(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/ARS-BRL/{quantity}")

        return response.json()

    def get_jpy(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/JPY-BRL/{quantity}")

        return response.json()

    def get_chf(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/CHF-BRL/{quantity}")

        return response.json()

    def get_cny(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/CNY-BRL/{quantity}")

        return response.json()

    def get_yls(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/YLS-BRL/{quantity}")

        return response.json()

    def get_bitcoin(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/BTC-BRL/{quantity}")

        return response.json()

    def get_litecoin(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/LTC-BRL/{quantity}")

        return response.json()

    def get_ethereum(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/ETH-BRL/{quantity}")

        return response.json()

    def get_ripple(self, format: str = "json", quantity: int = 1):
        response: Response = request("get", f"{_BASE_URL.format(target='economia')}/{format}/XRP-BRL/{quantity}")

        return response.json()
