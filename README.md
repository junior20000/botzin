# Botzin
> Projeto Integrado Uninove 6º Semestre

A simple multipurpose pt-BR chatbot with web interface.

## Running the tests
There is no tests to be ran ATM (*tests not implemented yet*).

## Deployment
No deployment instructions available for this project ATM.

## Built With
 * [Flask](https://palletsprojects.com/p/flask/) - [WSGI](https://palletsprojects.com/p/flask/) & Web Framework

## Contributing
The project is **NOT** open to contributions.

## Versioning
I use [GIT](https://git-scm.com/) for versioning.

## Author
 * [Roberto Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/) - September 2020

## Disclaimer
The ***Lon3 W0lf Corporation*** is just a joke (Lon3 W0lf is my project group name and the Corporation is a reference to the *Kaiba Corporation*).  
Consequently, I have no connections with that company if the same exists.

## Copyright
© Roberto Schiavelli Júnior - All Rights Reserved  
Unauthorized copying of this project and any files within it, via any medium is strictly prohibited  
Proprietary and confidential