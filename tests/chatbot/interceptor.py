#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 30/10/2020 09:29.

import unittest
from unittest.mock import patch

from chatbot.interceptor import *


@patch.object(IInterceptorManager, "__abstractmethods__", set())
class TestIInterceptorManager(unittest.TestCase):

    def test_isIterableAndOnlyContainsInterceptors_Interceptors_True(self):
        self.assertTrue(
            IInterceptorManager.is_iterable_and_only_contains_interceptors([InterceptorEmpty(), InterceptorEmpty()]))
        self.assertTrue(
            IInterceptorManager.is_iterable_and_only_contains_interceptors((InterceptorEmpty(), InterceptorEmpty())))
        self.assertTrue(
            IInterceptorManager.is_iterable_and_only_contains_interceptors({InterceptorEmpty(), InterceptorEmpty()}))

    def test_isIterableAndOnlyContainsInterceptors_NonInterceptors_False(self):
        self.assertFalse(IInterceptorManager.is_iterable_and_only_contains_interceptors([InterceptorEmpty(), 12]))
        self.assertFalse(IInterceptorManager.is_iterable_and_only_contains_interceptors([InterceptorEmpty(), "fail"]))
        self.assertFalse(
            IInterceptorManager.is_iterable_and_only_contains_interceptors([InterceptorEmpty(), [InterceptorEmpty()]]))
        self.assertFalse(IInterceptorManager.is_iterable_and_only_contains_interceptors("fail"))

    def test_instance_NoInterceptor_DefaultEmptyInterceptor(self):
        im = IInterceptorManager()

        self.assertIsInstance(im.interceptors[0], InterceptorEmpty)
        self.assertEqual(len(im.interceptors), 1)

    def test_instance_OneInterceptor_Instantiate(self):
        im = IInterceptorManager(InterceptorEmpty())

        self.assertIsInstance(im.interceptors[0], InterceptorEmpty)
        self.assertEqual(len(im.interceptors), 1)

    def test_instance_MultipleInterceptors_Instantiate(self):
        im = IInterceptorManager(InterceptorEmpty(), InterceptorEmpty())

        self.assertIsInstance(im, IInterceptorManager)
        self.assertEqual(len(im.interceptors), 2)

    def test_instance_NonInterceptor_RaiseException(self):
        with self.assertRaises(TypeError):
            IInterceptorManager(14)

    def test_instance_String_RaiseException(self):
        with self.assertRaises(TypeError):
            IInterceptorManager("this must fail")

    def test_instance_MultipleNonInterceptors_RaiseException(self):
        with self.assertRaises(TypeError):
            IInterceptorManager(InterceptorEmpty(), "this must fail", InterceptorEmpty())
        with self.assertRaises(TypeError):
            IInterceptorManager(InterceptorEmpty(), 12, InterceptorEmpty())
        with self.assertRaises(TypeError):
            IInterceptorManager(InterceptorEmpty(), [12, 15], InterceptorEmpty())
        with self.assertRaises(TypeError):
            IInterceptorManager(InterceptorEmpty(), [InterceptorEmpty(), InterceptorEmpty()], InterceptorEmpty())

    def test_interceptors_OneInterceptor_Add(self):
        im = IInterceptorManager(InterceptorEmpty())
        im.interceptors = InterceptorEmpty()

        self.assertEqual(len(im.interceptors), 2)

    def test_interceptors_IterablesOfInterceptors_Add(self):
        im = IInterceptorManager(InterceptorEmpty())
        im.interceptors = [InterceptorEmpty(), InterceptorEmpty()]
        im.interceptors = (InterceptorEmpty(), InterceptorEmpty())
        im.interceptors = {InterceptorEmpty(), InterceptorEmpty()}

        self.assertEqual(len(im.interceptors), 7)


if __name__ == "__main__":
    unittest.main()
