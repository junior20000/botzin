#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 28/10/2020 12:26.

import unittest

from chatbot_flask.misc import distance
from chatbot_flask.misc.exceptions import *


class TestDistanceCalculator(unittest.TestCase):
    dist_calc = distance.DistanceCalculator(-23.6103482, -46.6107422)

    def test_x_y_out_of_range(self):
        with self.assertRaises(LatitudeOutOfBoundsException):
            distance.DistanceCalculator(-123.6103482, -46.6107422)
        with self.assertRaises(LongitudeOutOfBoundsException):
            distance.DistanceCalculator(-23.6103482, -186.6107422)

    def test_distance_type(self):
        with self.assertRaises(TypeError):
            self.dist_calc.set_distance_type(float())

    def test_distance_type_not_set(self):
        with self.assertRaises(DistanceTypeNotSetException):
            self.dist_calc.get_distance((24, 25))

    def test_validate_coordinates(self):
        self.assertIs(self.dist_calc.validate_coordinates(-23.6103482, -46.6107422), None)
        self.assertIs(self.dist_calc.validate_coordinates(-23.6103482, -46.6107422, -29.6103482, -49.6107422), None)

    def test_validate_coordinates_out_of_bounds(self):
        with self.assertRaises(LatitudeOutOfBoundsException):
            self.dist_calc.validate_coordinates(-23.6103482, -46.6107422, -123.6103482, -46.6107422)
        with self.assertRaises(LongitudeOutOfBoundsException):
            self.dist_calc.validate_coordinates(-23.6103482, -186.6107422, -23.600177, -46.613692)

    def test_get_haversine_distance(self):
        self.dist_calc.set_distance_type(distance.HaversineDistance())
        d = self.dist_calc.get_distance((-23.600177, -46.613692))

        self.assertAlmostEqual(d, 1.17, 2)

    def test_get_new_haversine_distance(self):
        self.dist_calc.set_distance_type(distance.HaversineDistance())
        d = self.dist_calc.get_new_distance((-23.6103482, -46.6107422), (-23.600177, -46.613692))

        self.assertAlmostEqual(d, 1.17, 2)


class TestHaversineDistance(unittest.TestCase):
    haversine = distance.HaversineDistance()

    def test_hav(self):
        self.assertAlmostEqual( self.haversine.hav(-0.00017752092887888293), 7.878420e-09)

    def test_central_angle(self):
        h = self.haversine.central_angle(0.000177, -5.148372e-05, -0.412078, -0.411900)

        self.assertAlmostEqual(h, 8.434810e-09)

    def test_arcversin(self):
        self.assertEqual(self.haversine.arcversin(8.434810e-09), 0.00012988310108291278)

    def test_archaversin(self):
        self.assertEqual(self.haversine.archaversin(8.434810e-09), 0.00018368244380806664)

    def test_get_distance(self):
        # Short distance
        d = self.haversine.get_distance((-23.6103482, -46.6107422), (-23.600177, -46.613692))
        self.assertAlmostEqual(round(d, 2), 1.17)
        # Long distance
        d = self.haversine.get_distance((50.066389, -5.714722), (58.643889, -3.070000))
        self.assertAlmostEqual(round(d, 2), 968.85)

    def test_get_distance_lat_out_of_bounds(self):
        with self.assertRaises(LatitudeOutOfBoundsException):
            self.haversine.get_distance((90.066389, -5.714722), (58.643889, -3.070000))

    def test_get_distance_lng_out_of_bounds(self):
        with self.assertRaises(LongitudeOutOfBoundsException):
            self.haversine.get_distance((50.066389, -5.714722), (58.643889, -180.070000))

class TestSphericalLawOfCosinesDistance(unittest.TestCase):
    sloc = distance.SphericalLawOfCosinesDistance()

    def test_get_distance(self):
        d = self.sloc.get_distance((-23.6103482, -46.6107422), (-23.600177, -46.613692))

        self.assertAlmostEqual(d, 1.17, 2)

    def test_get_distance_lat_out_of_bounds(self):
        with self.assertRaises(LatitudeOutOfBoundsException):
            self.sloc.get_distance((90.066389, -5.714722), (58.643889, -3.070000))

    def test_get_distance_lng_out_of_bounds(self):
        with self.assertRaises(LongitudeOutOfBoundsException):
            self.sloc.get_distance((50.066389, -5.714722), (58.643889, -180.070000))

if __name__ == "__main__":
    unittest.main()