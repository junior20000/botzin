#  Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
#  Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
#  Proprietary and confidential
#  Written by Roberto Schiavelli Júnior
#  Last time modified: 30/10/2020 09:31.

import unittest

from chatbot_flask.interceptor import InterceptorManager, InterceptorCEP, InterceptorCurrency

class TestInterceptorManager(unittest.TestCase):
    interceptor_mng = InterceptorManager(InterceptorCEP(), InterceptorCurrency())

    def test_intercept_NoSuitableString_NoInterception(self):
        response = self.interceptor_mng.intercept("bom dia")
        self.assertIsNone(response)

    def test_intercept_StringWithDeliveryCEP_IsWithinBounds(self):
        response = self.interceptor_mng.intercept("Vocês entregariam no cep: 04276000")
        self.assertIn("está dentro", response)

    def test_intercept_StringWithCurrency_CurrencyQuotation(self):
        response = self.interceptor_mng.intercept("cotação dólar")
        self.assertIn("DÓLAR", response)


class TestInterceptorCEP(unittest.TestCase):
    interceptor = InterceptorCEP()

    def test_intercept_NoSuitableString_NoPatternMatch(self):
        response = self.interceptor.intercept("Vocês entreg no cep: 04288125?")
        self.assertIsNone(response)

    def test_intercept_StringWithDeliveryCEP_IsWithinBounds(self):
        response = self.interceptor.intercept("Vocês entregariam no cep: 04276000")
        self.assertIn("está dentro", response)

    def test_intercept_StringWithDeliveryCEP_IsOutsideBounds(self):
        response = self.interceptor.intercept("Vocês entregariam no cep: 69050000")
        self.assertIn("está fora", response)


class TestInterceptorCurrency(unittest.TestCase):
    interceptor = InterceptorCurrency()

    def test_intercept_NoPatternMatch(self):
        response = self.interceptor.intercept("cotacao dólar?")
        self.assertIsNone(response)

    def test_intercept_CurrencyNotFound(self):
        response = self.interceptor.intercept("cotação fleig")
        self.assertIn("Moeda não encontrada", response)

    def test_intercept_Dollar(self):
        response = self.interceptor.intercept("cotação dólar")
        self.assertIn("DÓLAR", response)

        response = self.interceptor.intercept("cotação dolar")
        self.assertIn("DOLAR", response)

    def test_intercept_Euro(self):
        response = self.interceptor.intercept("cotação euro")
        self.assertIn("EURO", response)

    def test_intercept_Bitcoin(self):
        response = self.interceptor.intercept("cotação bitcoin")
        self.assertIn("BITCOIN", response)

        response = self.interceptor.intercept("cotação btc")
        self.assertIn("BTC", response)


if __name__ == "__main__":
    unittest.main()
